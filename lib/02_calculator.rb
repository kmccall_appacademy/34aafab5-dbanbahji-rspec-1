def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(num_arr)
  return 0 if num_arr.empty?
  num_arr.reduce(:+)
end

def multiply(num1, num2)
  num1 * num2
end

def power(num, power)
  num ** power
end

def factorial(num)
  factorial = 1
  (1..num).each { |el| factorial *= el }
  factorial
end
