def echo(phrase)
  phrase
end


def shout(phrase)
  phrase.upcase
end

def repeat(phrase, num = 2)
  repeat = []
  num.times do
    repeat << phrase
  end
  repeat.join(' ')
end

def start_of_word(phrase, number_letters)
  phrase[0...number_letters]
end

def first_word(phrase)
  phrase.split.first
end

def titleize(phrase)
  little_words = ["the", "over", "and", "a"]
  split_phrase = phrase.split
  capitalized = split_phrase.map.with_index do |word,idx|
    if idx==0 || !little_words.include?(word)
      word.capitalize
    else
      word
    end
  end

  capitalized.join(" ")
end
